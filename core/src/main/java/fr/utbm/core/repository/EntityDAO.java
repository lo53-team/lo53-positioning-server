package fr.utbm.core.repository;

import fr.utbm.core.entity.CoreEntity;
import fr.utbm.core.tools.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <b>Abstract class DAO to add a genericity layer to the entities DAO</b>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public abstract class EntityDAO {

    protected Session session;

    /**
     *
     * @return
     */
    public Session getSession() {

        if ( this.session == null || !this.session.isConnected() ) {
            this.session = HibernateUtil.getSessionFactory().openSession();
        }
        return session;
    }

    /**
     *
     * @param session
     */
    public void setSession(Session session) {
        this.session = session;
    }

    /**
     *
     */
    public void closeSession() {
        if (this.session != null) {
            try {
                this.session.close();
            } catch(HibernateException he) {

            }
        }
    }

    public void delete(CoreEntity e){

        try {
            this.getSession().beginTransaction();

            this.getSession().delete(e);

            this.getSession().getTransaction().commit();
        } catch( HibernateException he) {
            he.printStackTrace();
        } finally {
            this.closeSession();
        }

    }
}
