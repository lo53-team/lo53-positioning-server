package fr.utbm.core.repository;

import fr.utbm.core.entity.Location;
import fr.utbm.core.entity.RSSIHistogram;
import fr.utbm.core.entity.RSSIOccurence;
import fr.utbm.core.utils.Log;
import org.hibernate.HibernateException;
import org.hibernate.Query;

import java.util.*;


/**
 * <b>Data Access Object used to manager RSSIHistogram entities</b>
 * <p>Only a few methods are implemented because we didn't need all CRUD operations</p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class RSSIHistogramDao extends EntityDAO {

	/**
	 * The class name used in the logs
	 */
	public final static String CLASS_NAME = RSSIHistogramDao.class.getSimpleName();

	/**
	 *	This method fetch the list of all histogramfingerprint in the database and parse them to return a dictionary Location -> List
	 *
	 * @param mapId map identifier
	 * @return a key value pair with Location as key and a list of RSSIHistogram as value
	 */
	public HashMap<Location, ArrayList<RSSIHistogram>> getHistogramFingerprints(long mapId) {

		HashMap<Location, ArrayList<RSSIHistogram>> histogramFigerPrints = new HashMap<Location, ArrayList<RSSIHistogram>>();

		List<RSSIHistogram> rssiHistograms = new ArrayList<RSSIHistogram>();

		try {

			//Query query = this.getSession().createQuery("select new map(loc, rh) from RSSIHistogram as rh inner join fetch rh.fingerPrintHistogram as loc inner join fetch loc.map as map where map.id = " + mapId + " group by loc");
			Query query = this.getSession().createQuery("from RSSIHistogram as rh inner join fetch rh.fingerPrintHistogram as loc inner join fetch loc.map as map inner join fetch rh.accessPoint inner join fetch rh.rssiOccurences where map.id = " + mapId);

			rssiHistograms = query.list();

		} catch( HibernateException he) {
		} finally {
			this.closeSession();
		}

		for(RSSIHistogram rssiHistogram : rssiHistograms) {

			Location location = rssiHistogram.getFingerPrintHistogram();
			if(histogramFigerPrints.get(location) == null) {
				histogramFigerPrints.put(location, new ArrayList<RSSIHistogram>());
			}

			histogramFigerPrints.get(location).add(rssiHistogram);
		}


		return histogramFigerPrints;
	}

	/**
	 * Persist every RSSIHistogram present on the list called rssiHistograms
	 * @param rssiHistograms list of RSSIHistogram object
     */
	public void save(ArrayList<RSSIHistogram> rssiHistograms) {

		try {
			this.getSession().beginTransaction();

			for(RSSIHistogram rssiHistogram : rssiHistograms) {
				this.getSession().save(rssiHistogram.getFingerPrintHistogram());
				this.getSession().save(rssiHistogram);
			}

			this.getSession().getTransaction().commit();
		} catch( HibernateException he) {
			he.printStackTrace();
		} finally {
			this.closeSession();
		}

	}


	/**
	 * Fetch the list of all RSSIHistogram associated with the referecne point Location on the map of the ID mapId
	 *
	 * @param mapId map identifier
	 * @param location location of a reference point
	 * @return all RSSIHistogram associated with the referecne point Location on the map of the ID mapId
	 */
	public List<RSSIHistogram> getHistogramFingerprints(long mapId, Location location) {

		List<RSSIHistogram> rssiHistograms = new ArrayList<RSSIHistogram>();

		try {

			//Query query = this.getSession().createQuery("select new map(loc, rh) from RSSIHistogram as rh inner join fetch rh.fingerPrintHistogram as loc inner join fetch loc.map as map where map.id = " + mapId + " group by loc");
			Query query = this.getSession().createQuery("from RSSIHistogram as rh inner join fetch rh.fingerPrintHistogram as loc inner join fetch loc.map as map inner join fetch rh.accessPoint as ap inner join fetch rh.rssiOccurences where map.id = " + mapId + " and loc.coordinateX = " + location.getCoordinateX() + " and loc.coordinateY = " + location.getCoordinateY());

			rssiHistograms = query.list();

		} catch (HibernateException he) {
		} finally {
			this.closeSession();
		}

		return rssiHistograms;
	}

	/**
	 * Delete every RSSI histogram of a reference point from the database
	 *
	 * @param mapId map identifier
	 * @param location location of a reference point
	 */
	public void deleteFromPreviousCalibration(int mapId, Location location) {

		List<RSSIHistogram> rssiHistograms = this.getHistogramFingerprints(mapId, location);

		Log.info(CLASS_NAME, Arrays.toString(rssiHistograms.toArray()));

		for(RSSIHistogram rssiHistogram : rssiHistograms) {
			this.delete(rssiHistogram);
		}

	}
}
