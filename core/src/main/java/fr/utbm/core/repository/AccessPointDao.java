package fr.utbm.core.repository;

import fr.utbm.core.entity.AccessPoint;
import fr.utbm.core.entity.Map;
import org.hibernate.HibernateException;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Data Access Object used to manager AccessPoint entities</b>
 * <p>Only a few methods are implemented because we didn't need all CRUD operations</p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class AccessPointDao extends EntityDAO {

	/**
	 * Return the full list of DAO
	 * @return
	 */
	public List<AccessPoint> list() {

		List<AccessPoint> accessPoints = new ArrayList();

		try {

			accessPoints = this.getSession().createCriteria(AccessPoint.class).list();

		} catch( HibernateException he) {

		} finally {
			this.closeSession();
		}

		return accessPoints;
	}

	/**
	 * Get map object from ID
	 * @param id map id
	 * @return Map | null
     */
	public AccessPoint get(long id) {

		AccessPoint accessPoint = null;

		try {

			accessPoint = (AccessPoint) this.getSession().get(AccessPoint.class, id);

		} catch( HibernateException he) {
			System.err.println(he.getMessage());
		} finally {
			this.closeSession();
		}

		return accessPoint;
	}
}
