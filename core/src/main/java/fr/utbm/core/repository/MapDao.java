package fr.utbm.core.repository;

import fr.utbm.core.entity.Map;
import org.hibernate.HibernateException;

import java.util.List;


/**
 * <b>Data Access Object used to manager Map entities</b>
 * <p>Only a few methods are implemented because we didn't need all CRUD operations</p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class MapDao extends EntityDAO {

	/**
	 * Return the full list of DAO
	 * @return
	 */
	public List<Map> list() {

		List<Map> maps = null;

		try {

			maps = (List<Map>) this.getSession().createCriteria(Map.class).list();

		} catch( HibernateException he) {

		} finally {
			this.closeSession();
		}

		return maps;
	}

	/**
	 * Get map object from ID
	 * @param id map id
	 * @return Map | null
     */
	public Map get(long id) {

		Map map = null;

		try {

			map = (Map) this.getSession().get(Map.class, id);

		} catch( HibernateException he) {
			System.err.println(he.getMessage());
		} finally {
			this.closeSession();
		}

		return map;
	}
}
