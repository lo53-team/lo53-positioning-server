package fr.utbm.core.service;

import fr.utbm.core.entity.Location;
import fr.utbm.core.entity.RSSIHistogram;
import fr.utbm.core.entity.RSSIOccurence;
import fr.utbm.core.repository.RSSIHistogramDao;
import fr.utbm.core.utils.LocationUtils;
import fr.utbm.core.utils.Log;

import java.util.*;

public class LocationService {

	public final static String CLASS_NAME = "LocationService";

	/**
	 * Compute the device location from mapId and macAddress
	 *
	 * @param mapId mapIdentifier
	 * @param macAddress device macAddress
     * @return Location | null
     */
	public Location getDeviceLocation(int mapId, String macAddress) {

		Log.info(CLASS_NAME, "Start getDeviceLocation method");

		AccessPointService accessPointService = new AccessPointService();
		ArrayList<RSSIHistogram> rssiHistogramsSample = accessPointService.getRssiMeasurements(macAddress);
		//ArrayList<RSSIHistogram> rssiHistogramsSample = getMockedRSSIHistogram();

		Log.info(CLASS_NAME, "rssiHistogramsSample count : " + rssiHistogramsSample.size());

		// Getting histogram finger print, every location => rssiHistogramSample
		RSSIHistogramDao rssiHistogramDao = new RSSIHistogramDao();
		HashMap<Location, ArrayList<RSSIHistogram>> rssiHistogramFingerprints = rssiHistogramDao.getHistogramFingerprints(mapId);

		Log.info(CLASS_NAME, "rssiHistogramFingerprints count : " + rssiHistogramFingerprints.size());

		Double probabilityValue = 0.0;
		Location location 	    = null;

		Iterator it = rssiHistogramFingerprints.entrySet().iterator();
		while (it.hasNext()) {

			Log.info(CLASS_NAME, "Iteration rssiHistogramFingerprints");

			Map.Entry pair = (Map.Entry) it.next();

			Double currentProbabilityValue = this.probability(rssiHistogramsSample, (ArrayList<RSSIHistogram>) pair.getValue());

			Log.info(CLASS_NAME, "currentProbabilityValue : " + currentProbabilityValue);

			if(probabilityValue < currentProbabilityValue) {
				probabilityValue = currentProbabilityValue;
				location = (Location) pair.getKey();
			}

			it.remove();
		}

		Log.info(CLASS_NAME, "End getDeviceLocation method");

		return location;
	}

	/**
	 *
	 *
	 * @param current first list of RSSIHistogram
	 * @param fingerprint second list of RSSIHistogram
     * @return the probability that the first list match the second
     */
	private Double probability(ArrayList<RSSIHistogram> current, ArrayList<RSSIHistogram> fingerprint) {

		Double probabilityValue = 1.0;

		// TODO need improvements !!!!! LocationService : macAddresses : [F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:43, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:44, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47, F8:D3:7R:54:47]
		ArrayList<String> macAddresses = LocationUtils.getMacAddressesFromRssiSample(fingerprint);

		Log.info(CLASS_NAME, "macAddresses : " + Arrays.toString(macAddresses.toArray()));

		for (RSSIHistogram rssiHistogram : current) {

			Log.info(CLASS_NAME, "current macaddress : " + rssiHistogram.getAccessPoint().getMacAddress());

			// Finding mac address in current
			if(macAddresses.contains(rssiHistogram.getAccessPoint().getMacAddress())) {

				probabilityValue = probabilityValue * probabilityHistogram(
						rssiHistogram.getRssiOccurences(),
						LocationUtils.getRSSIOccurencesFromRssiSample(fingerprint, rssiHistogram.getAccessPoint().getMacAddress())
				);

			} else {
				// TODO research...
				probabilityValue = 0.0;
			}

			Log.info(CLASS_NAME, "current probability value : " + probabilityValue);

		}

		return probabilityValue;
	}

	/**
	 *
	 * @param sample1 first set of RSSIOccurence
	 * @param sample2 second set of RSSIOccurence
     * @return the probability that the first set match the second
     */
	private Double probabilityHistogram(Set<RSSIOccurence> sample1, Set<RSSIOccurence> sample2) {

		Double sumSample1 	= 0.0;
		Double sumSample2 	= 0.0;
		Double probability 	= 0.0;

		for(RSSIOccurence rssiOccurence : sample1) {
			sumSample1 = sumSample1 + rssiOccurence.getOccurences();
		}

		for(RSSIOccurence rssiOccurence : sample2) {
			sumSample2 = sumSample2 + rssiOccurence.getOccurences();
		}

		for(RSSIOccurence rssiOccurence : sample1) {
			if(LocationUtils.getRssiValuesFromRssiOccurences(sample2).contains(rssiOccurence.getValue())) {

				try {
					probability = probability + Math.min(
							rssiOccurence.getOccurences() / sumSample1,
							LocationUtils.getRssiValueFromRssiOccurences(sample2, rssiOccurence.getValue()).getOccurences() / sumSample2
					);
				} catch(Exception e) {
					System.err.println(e.getMessage());
				}

			}
		}

		Log.info(CLASS_NAME, "probabilityHistogram probability value : " + probability);

		return probability;
	}

}
