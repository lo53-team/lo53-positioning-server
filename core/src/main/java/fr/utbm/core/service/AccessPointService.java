package fr.utbm.core.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import fr.utbm.core.entity.AccessPoint;
import fr.utbm.core.entity.RSSIHistogram;
import fr.utbm.core.entity.RSSIOccurence;
import fr.utbm.core.repository.AccessPointDao;
import fr.utbm.core.utils.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;

public class AccessPointService {

	public static String TAG = "AccessPointService";

	/**
	 * Get the list of RSSI histogram measured by AP
	 *
	 * @param macAddress macAddress of a device
	 * @return the list of RSSI histogram measured by AP
	 */
	public ArrayList<RSSIHistogram> getRssiMeasurements(String macAddress) {

		ArrayList<RSSIHistogram> rssiHistograms = new ArrayList<RSSIHistogram>();

		// fetching access point list from DB
		AccessPointDao accessPointDao = new AccessPointDao();
		List<AccessPoint> accessPoints = accessPointDao.list();

		// Send each request to APs
		for(AccessPoint accessPoint : accessPoints) {

			Log.info(TAG, "Sending request to AP : " + buildAccessPointUrl(accessPoint, macAddress));

			// Parse JSON response
			try {

				RestTemplate restTemplate = new RestTemplate();
				ResponseEntity<String> response = restTemplate.getForEntity(
						buildAccessPointUrl(accessPoint, macAddress),
						String.class
				);

				Log.info(TAG, "Response from AP : " + response.getBody());

				ObjectMapper mapper = new ObjectMapper();
				JsonNode value = mapper.readTree(response.getBody());

				RSSIHistogram rssiHistogram = parseRSSIValues(accessPoint, value);
				rssiHistograms.add(rssiHistogram);
			} catch (IOException e) {
				System.err.println(e.getMessage());
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}

		return rssiHistograms;
	}

	/**
	 * Create RSSIHistogram from jsonValue fetch from AP
	 *
	 * @param accessPoint access point entity
	 * @param jsonValue json value containing RSSI informations
     * @return RSSIHistogram
     */
	public RSSIHistogram parseRSSIValues(AccessPoint accessPoint, JsonNode jsonValue) throws Exception {

		RSSIHistogram rssiHistogram = new RSSIHistogram();
		rssiHistogram.setAccessPoint(accessPoint);

		JsonNode jsonValues = jsonValue.get("values");

		if(jsonValues.getNodeType() != JsonNodeType.ARRAY) {
			throw new Exception("JSON values is not an array");
		}

		HashMap<Double, Integer> occurences = new HashMap<Double, Integer>();

		for(JsonNode jsonRssiValue : jsonValues) {

			if(occurences.get(jsonRssiValue.doubleValue()) != null) {
				occurences.put(
						jsonRssiValue.doubleValue(),
						occurences.get(jsonRssiValue.doubleValue()) + 1
						);
			} else {
				occurences.put(jsonRssiValue.doubleValue(), 1);
			}

		}

		Set<RSSIOccurence> rssiOccurences = new HashSet<RSSIOccurence>();

		for(Map.Entry<Double, Integer> entry : occurences.entrySet()) {
			RSSIOccurence rssiOccurence = new RSSIOccurence(entry.getKey(), entry.getValue());
			rssiOccurence.setRssiHistogram(rssiHistogram);
			rssiOccurences.add(rssiOccurence);
		}

		rssiHistogram.setRssiOccurences(rssiOccurences);

		return rssiHistogram;
	}

	/**
	 * Get the full URL to send to an access point
	 *
	 * @param accessPoint accesspoint address
	 * @param macAddress macAddress of a device
	 * @return an access point url
	 */
	private String buildAccessPointUrl(AccessPoint accessPoint, String macAddress) {
		return "http://" + accessPoint.getIpAddress() + "/" + macAddress;
	}

}
