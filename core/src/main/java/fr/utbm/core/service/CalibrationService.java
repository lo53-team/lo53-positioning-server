package fr.utbm.core.service;

import fr.utbm.core.entity.Location;
import fr.utbm.core.entity.Map;
import fr.utbm.core.entity.RSSIHistogram;
import fr.utbm.core.repository.MapDao;
import fr.utbm.core.repository.RSSIHistogramDao;
import fr.utbm.core.tools.HibernateUtil;
import fr.utbm.core.utils.Log;
import org.hibernate.Session;
import sun.util.resources.cldr.ga.LocaleNames_ga;

import java.util.ArrayList;

public class CalibrationService {

	public final static String TAG = CalibrationService.class.getSimpleName();

	/**
	 * @param mapId map identifer
	 * @param macAddress macAddress of a device who make the calibration
	 * @param location location
	 * @return a Location object if the calibration was successful, otherwise return null
	 */
	public Location calibrateLocation(int mapId, String macAddress, Location location) {

		Log.info(TAG, "Start calibrateLocation");

		this.removePreviousLocation(mapId, location);

		// Find map
		MapDao mapDao = new MapDao();
		Map map = mapDao.get(mapId);

		// Request each access points with macAddress to fetch RSSIs values;
		AccessPointService accessPointService = new AccessPointService();
		ArrayList<RSSIHistogram> rssiHistograms = accessPointService.getRssiMeasurements(macAddress);

		location.setMap(map);

		for (RSSIHistogram rssiHistogram : rssiHistograms) {
			rssiHistogram.setFingerPrintHistogram(location);

			Log.info(TAG, rssiHistogram.toString());
		}

		if (rssiHistograms.isEmpty()) {
			location = null;
		} else {

			// Persist theses values
			RSSIHistogramDao rssiHistogramDao = new RSSIHistogramDao();
			rssiHistogramDao.save(rssiHistograms);
		}

		Log.info(TAG, "End calibrateLocation");

		return location;
	}

	/**
	 * Delete all previous RSSIHistograms of a reference point
	 *
	 * @param mapId map identifier
	 * @param location location reference point
	 */
	public void removePreviousLocation(int mapId, Location location) {

		RSSIHistogramDao rssiHistogramDao = new RSSIHistogramDao();
		rssiHistogramDao.deleteFromPreviousCalibration(mapId, location);

	}
}