package fr.utbm.core.entity;

/**
 * <b>Abstract class Entity</b>
 * <p>
 *     The main use is to make some function generic like the entity removing.
 * </p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public abstract class CoreEntity {

    public CoreEntity() {}
}
