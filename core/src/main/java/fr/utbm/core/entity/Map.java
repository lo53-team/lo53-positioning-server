package fr.utbm.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * <b>Entity that represent a Map</b>
 * <p>
 *     Each properties are mapped to an table field in our database.
 *     It can be persisted on the DB with an Hibernate DAO
 * </p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
@Entity
@Table(name="map")
public class Map extends CoreEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "map", cascade = CascadeType.ALL)
    private Set<Location> locations;

    public Map() {
        this.locations = new HashSet<Location>();
    }

    public Map(String name) {
        this.name = name;
        this.locations = new HashSet<Location>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

    @Override
    public String toString() {
        return "Map{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
