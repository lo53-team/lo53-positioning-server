package fr.utbm.core.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * <b>Entity that represent RSSIHistogram</b>
 * <p>
 *     Each properties are mapped to an table field in our database.
 *     It can be persisted on the DB with an Hibernate DAO
 * </p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
@Entity
@Table(name="rssi_histogram")
public class RSSIHistogram extends CoreEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    /**
     * List RSSI Occurences
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "rssiHistogram")
    private Set<RSSIOccurence> rssiOccurences;

    /**
     * Access point
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private AccessPoint accessPoint;

    /**
     * Location fingerprint histogram
     */
    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "fingerprint_histogram_id")
    private Location fingerPrintHistogram;

    public RSSIHistogram() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public AccessPoint getAccessPoint() {
        return accessPoint;
    }

    public void setAccessPoint(AccessPoint accessPoint) {
        this.accessPoint = accessPoint;
    }

    public Location getFingerPrintHistogram() {
        return fingerPrintHistogram;
    }

    public void setFingerPrintHistogram(Location fingerPrintHistogram) {
        this.fingerPrintHistogram = fingerPrintHistogram;
    }

    public Set<RSSIOccurence> getRssiOccurences() {
        return rssiOccurences;
    }

    public void setRssiOccurences(Set<RSSIOccurence> rssiOccurences) {
        this.rssiOccurences = rssiOccurences;
    }

    @Override
    public String toString() {
        return "RSSIHistogram{" +
                "id=" + id +
                ", rssiOccurences=" + rssiOccurences +
                ", fingerPrintHistogram=" + fingerPrintHistogram +
                '}';
    }
}
