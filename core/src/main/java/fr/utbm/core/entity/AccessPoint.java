package fr.utbm.core.entity;


import javax.persistence.*;

/**
 * <b>Entity that represent AccessPoint</b>
 * <p>
 *     Each properties are mapped to an table field in our database.
 *     It can be persisted on the DB with an Hibernate DAO
 * </p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
@Entity
@Table(name="access_point")
public class AccessPoint extends CoreEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "mac_address", nullable = false)
    private String macAddress;

    @Column(name = "ip_address", nullable = false)
    private String ipAddress;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "location")
    private Location location;


    /**
     * Default constructor
     */
    public AccessPoint() {
        super();
    }

    /**
     *
     * @param macAddress macAddress of the AP
     * @param ipAddress ipAddress and port of the AP
     * @param location location object representing the position of the AP in the room

     */
    public AccessPoint(String macAddress, String ipAddress, Location location) {
        this.macAddress     = macAddress;
        this.ipAddress      = ipAddress;
        this.location       = location;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "AccessPoint{" +
                "id=" + id +
                ", macAddress='" + macAddress + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", location=" + location +
                '}';
    }
}