package fr.utbm.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

/**
 * <b>Entity that represent Location</b>
 * <p>
 *     Each properties are mapped to an table field in our database.
 *     It can be persisted on the DB with an Hibernate DAO
 * </p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
@Entity
@Table(name="location")
public class Location extends CoreEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonIgnore
    private long id;

    @Column(name = "coordinate_x", nullable = false)
    private int coordinateX;

    @Column(name = "coordinate_y", nullable = false)
    private int coordinateY;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JsonIgnore
    private Map map;

    public Location() {}

    public Location(int coordinateX, int coordinateY, Map map) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.map = map;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", coordinateX='" + coordinateX + '\'' +
                ", coordinateY='" + coordinateY + '\'' +
                ", map=" + map.getId() +
                '}';
    }
}
