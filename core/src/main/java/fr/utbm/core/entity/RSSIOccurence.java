package fr.utbm.core.entity;

import javax.persistence.*;

/**
 * <b>Entity that represent RSSIOccurence</b>
 * <p>
 *     Each properties are mapped to an table field in our database.
 *     It can be persisted on the DB with an Hibernate DAO
 * </p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
@Entity
@Table(name="rssi_occurence")
public class RSSIOccurence extends CoreEntity {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "value", nullable = false)
    private double value;

    @Column(name = "occurences", nullable = false)
    private int occurences;

    @ManyToOne
    private RSSIHistogram rssiHistogram;

    public RSSIOccurence() {}

    public RSSIOccurence(double value, int occurences) {
        this.value = value;
        this.occurences = occurences;
    }

    public RSSIOccurence(double value, int occurences, RSSIHistogram rssiHistogram) {
        this(value, occurences);
        this.rssiHistogram = rssiHistogram;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getOccurences() {
        return occurences;
    }

    public void setOccurences(int occurences) {
        this.occurences = occurences;
    }

    public RSSIHistogram getRssiHistogram() {
        return rssiHistogram;
    }

    public void setRssiHistogram(RSSIHistogram rssiHistogram) {
        this.rssiHistogram = rssiHistogram;
    }
}
