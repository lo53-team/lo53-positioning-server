package fr.utbm.core.utils;

import fr.utbm.core.entity.RSSIHistogram;
import fr.utbm.core.entity.RSSIOccurence;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * <b>Some useful functions about RSSIsample</b>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class LocationUtils {

    /**
     * Convert a list of RSSIHistogram to a list of access points macAddresses
     *
     * @param measurements list of RSSIHistogram
     * @return a list of access points macAddresses
     */
    public static ArrayList<String> getMacAddressesFromRssiSample(ArrayList<RSSIHistogram> measurements) {
        ArrayList<String> macAddresses = new ArrayList<String>();
        for(RSSIHistogram rssiHistogram : measurements) {
            macAddresses.add(rssiHistogram.getAccessPoint().getMacAddress());
        }
        return macAddresses;
    }

    /**
     * Filter a list of RSSIHistogram by a macAddress
     *
     * @param measurements list of RSSIHistogram
     * @param macAddress AP macAddress
     * @return a set of RSSIOccurence
     */
    public static Set<RSSIOccurence> getRSSIOccurencesFromRssiSample(ArrayList<RSSIHistogram> measurements, String macAddress) {
        Set<RSSIOccurence> rssiOccurences = new HashSet<RSSIOccurence>();

        for(RSSIHistogram rssiHistogram : measurements) {
            if(macAddress.equals(rssiHistogram.getAccessPoint().getMacAddress())) {
                rssiOccurences = rssiHistogram.getRssiOccurences();
            }
        }

        return rssiOccurences;
    }

    /**
     * Convert a set of RSSIOccurence to an array of RSSI values
     *
     * @param rssiOccurences set of RSSIOccurence
     * @return an array of Double value fetch from a set of RSSIOccurence
     */
    public static ArrayList<Double> getRssiValuesFromRssiOccurences(Set<RSSIOccurence> rssiOccurences) {
        ArrayList<Double> values = new ArrayList<Double>();

        for (RSSIOccurence rssiOccurence : rssiOccurences) {
            values.add(rssiOccurence.getValue());
        }

        return values;
    }

    /**
     * Get an RSSIOccuence which match the value
     *
     * @param rssiOccurences  set of RSSIOccurence
     * @param value RSSI value
     * @return RSSIOccurence which match the value
     */
    public static RSSIOccurence getRssiValueFromRssiOccurences(Set<RSSIOccurence> rssiOccurences, Double value) throws Exception {

        for (RSSIOccurence currentRssiOccurence : rssiOccurences) {
            if(currentRssiOccurence.getValue() == value) {
                return currentRssiOccurence;
            }
        }

        throw new Exception("LocationUtils.getRssiValueFromRssiOccurences : RSSI occurence not found");
    }
}
