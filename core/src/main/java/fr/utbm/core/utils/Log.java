package fr.utbm.core.utils;

/**
 * Created by lo53 on 27/05/16.
 */
public class Log {

    public static void info(String classname, String message) {
        System.out.println(classname + " : " + message);
    }

    public static void error(String classname, String message) {
        System.err.println(classname + " : " + message);
    }



}
