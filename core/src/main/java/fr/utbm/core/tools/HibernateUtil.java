package fr.utbm.core.tools;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

/**
 * <b>Starting point to the Hibernate ORM</b>
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class HibernateUtil {

    private static AnnotationConfiguration annotationConfiguration  = new AnnotationConfiguration();
    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            return annotationConfiguration.configure().buildSessionFactory();
        } catch (Exception ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }

        return sessionFactory;
    }

    public static void setAnnotationConfiguration( AnnotationConfiguration annotationConfiguration ) {
        HibernateUtil.annotationConfiguration = annotationConfiguration;
    }

}