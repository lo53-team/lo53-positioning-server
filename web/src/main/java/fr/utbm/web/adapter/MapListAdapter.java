package fr.utbm.web.adapter;

import fr.utbm.core.entity.Map;

import java.util.ArrayList;
import java.util.List;

/**
 * Class use by the JSON parser to generate the response of a map list send by the positioningController. It's a JSON wrapper.
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class MapListAdapter {

    private List<Map> maps;

    public MapListAdapter(List<Map> maps) {
        this.maps = maps;
    }

    public List<Map> getMaps() {
        return maps;
    }

    public void setMaps(List<Map> maps) {
        this.maps = maps;
    }
}
