package fr.utbm.web.adapter;

import fr.utbm.core.entity.Location;

/**
 * Class use by the JSON parser to generate the response of a location send by the positioningController. It's a JSON wrapper.
 *
 * @author Julien PETIT
 * @version 1.0
 */
public class LocationAdapter {

    private Location location;

    public LocationAdapter(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
