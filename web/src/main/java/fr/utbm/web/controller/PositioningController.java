package fr.utbm.web.controller;

import fr.utbm.core.entity.Location;
import fr.utbm.core.repository.MapDao;
import fr.utbm.core.service.CalibrationService;
import fr.utbm.core.service.LocationService;
import fr.utbm.web.adapter.LocationAdapter;
import fr.utbm.web.adapter.MapListAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * This class will handle three HTTP requests
 * <ul>
 *     <li>Location request</li>
 *     <li>Calibration request</li>
 *     <li>Map list request</li>
 * </ul>
 *
 * <p>Each method send a class called Adapter, that will be trapped by the JSON parser en send back to the client.</p>
 *
 * @author Julien PETIT
 * @version 1.0
 */
@Controller
@EnableWebMvc
public class PositioningController {

    /**
     * Handle calibration request
     *
     * @param mapId identifier of the map that we are working on
     * @param macAddress macAddress of the device who request the calibration
     * @param coordinateX coordinate X of the reference point to calibrate
     * @param coordinateY coordinate Y of the reference point to calibrate
     * @return a JSON Location object wrapped in LocationAdapter class
     */
    @RequestMapping(value="/calibrate", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody LocationAdapter Calibrate(
            @RequestParam("map_id") int mapId,
            @RequestParam("mac_address") String macAddress,
            @RequestParam("x") int coordinateX,
            @RequestParam("y") int coordinateY) {

        CalibrationService calibrationService = new CalibrationService();

        return new LocationAdapter(
                calibrationService.calibrateLocation(mapId, macAddress, new Location(coordinateX, coordinateY, null))
        );
    }

    /**
     * Handle location request
     *
     * @param mapId identifier of the map that we are working on
     * @param macAddress macAddress of the device who request the location
     * @return a JSON Location object wrapped in LocationAdapter class, the location wrapped might be null if the location isn't found
     */
    @RequestMapping(value="/locate", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody LocationAdapter Locate(@RequestParam("map_id") int mapId, @RequestParam("mac_address") String macAddress) {

        // Fetching mac address device
        System.out.println("mapId : " + mapId);

        // Fetching map id
        System.out.println("macAddress : " + macAddress);

        LocationService locationService = new LocationService();

        return new LocationAdapter(locationService.getDeviceLocation(mapId, macAddress));
    }

    /**
     * Handle maplist request
     *
     * @return a JSON map list wrapped in a MapListAdapter
     */
    @RequestMapping(value="/maplist", method = RequestMethod.GET, produces="application/json")
    public @ResponseBody MapListAdapter getMapList() {

        MapDao mapDao = new MapDao();

        MapListAdapter mapListAdapter = new MapListAdapter(mapDao.list());

        return mapListAdapter;
    }

}