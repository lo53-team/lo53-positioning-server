var annotated_dup =
[
    [ "fr", null, [
      [ "utbm", null, [
        [ "core", null, [
          [ "entity", null, [
            [ "AccessPoint", "classfr_1_1utbm_1_1core_1_1entity_1_1AccessPoint.html", "classfr_1_1utbm_1_1core_1_1entity_1_1AccessPoint" ],
            [ "CoreEntity", "classfr_1_1utbm_1_1core_1_1entity_1_1CoreEntity.html", "classfr_1_1utbm_1_1core_1_1entity_1_1CoreEntity" ],
            [ "Location", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html", "classfr_1_1utbm_1_1core_1_1entity_1_1Location" ],
            [ "Map", "classfr_1_1utbm_1_1core_1_1entity_1_1Map.html", "classfr_1_1utbm_1_1core_1_1entity_1_1Map" ],
            [ "RSSIHistogram", "classfr_1_1utbm_1_1core_1_1entity_1_1RSSIHistogram.html", "classfr_1_1utbm_1_1core_1_1entity_1_1RSSIHistogram" ],
            [ "RSSIOccurence", "classfr_1_1utbm_1_1core_1_1entity_1_1RSSIOccurence.html", "classfr_1_1utbm_1_1core_1_1entity_1_1RSSIOccurence" ]
          ] ],
          [ "repository", null, [
            [ "AccessPointDao", "classfr_1_1utbm_1_1core_1_1repository_1_1AccessPointDao.html", "classfr_1_1utbm_1_1core_1_1repository_1_1AccessPointDao" ],
            [ "EntityDAO", "classfr_1_1utbm_1_1core_1_1repository_1_1EntityDAO.html", "classfr_1_1utbm_1_1core_1_1repository_1_1EntityDAO" ],
            [ "MapDao", "classfr_1_1utbm_1_1core_1_1repository_1_1MapDao.html", "classfr_1_1utbm_1_1core_1_1repository_1_1MapDao" ],
            [ "RSSIHistogramDao", "classfr_1_1utbm_1_1core_1_1repository_1_1RSSIHistogramDao.html", "classfr_1_1utbm_1_1core_1_1repository_1_1RSSIHistogramDao" ]
          ] ],
          [ "service", null, [
            [ "AccessPointService", "classfr_1_1utbm_1_1core_1_1service_1_1AccessPointService.html", "classfr_1_1utbm_1_1core_1_1service_1_1AccessPointService" ],
            [ "CalibrationService", "classfr_1_1utbm_1_1core_1_1service_1_1CalibrationService.html", "classfr_1_1utbm_1_1core_1_1service_1_1CalibrationService" ],
            [ "LocationService", "classfr_1_1utbm_1_1core_1_1service_1_1LocationService.html", "classfr_1_1utbm_1_1core_1_1service_1_1LocationService" ]
          ] ],
          [ "tools", null, [
            [ "HibernateUtil", "classfr_1_1utbm_1_1core_1_1tools_1_1HibernateUtil.html", null ]
          ] ],
          [ "utils", null, [
            [ "LocationUtils", "classfr_1_1utbm_1_1core_1_1utils_1_1LocationUtils.html", null ],
            [ "Log", "classfr_1_1utbm_1_1core_1_1utils_1_1Log.html", null ]
          ] ]
        ] ],
        [ "web", null, [
          [ "adapter", null, [
            [ "LocationAdapter", "classfr_1_1utbm_1_1web_1_1adapter_1_1LocationAdapter.html", "classfr_1_1utbm_1_1web_1_1adapter_1_1LocationAdapter" ],
            [ "MapListAdapter", "classfr_1_1utbm_1_1web_1_1adapter_1_1MapListAdapter.html", "classfr_1_1utbm_1_1web_1_1adapter_1_1MapListAdapter" ]
          ] ],
          [ "controller", null, [
            [ "PositioningController", "classfr_1_1utbm_1_1web_1_1controller_1_1PositioningController.html", "classfr_1_1utbm_1_1web_1_1controller_1_1PositioningController" ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "CalibrationTest", "classCalibrationTest.html", "classCalibrationTest" ]
];