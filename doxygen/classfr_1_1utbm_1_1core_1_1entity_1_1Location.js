var classfr_1_1utbm_1_1core_1_1entity_1_1Location =
[
    [ "Location", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a235534f514f05fdf215ae0f1be970de1", null ],
    [ "Location", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a6c97671f48cf1d5417e6db948113f9b0", null ],
    [ "getCoordinateX", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a8571b26f0eb9dd9595270b39abbbcaf8", null ],
    [ "getCoordinateY", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a8ff5453f550589c4f73df9e768a44cfc", null ],
    [ "getId", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#ab66eee562d35a31760c9646b10611873", null ],
    [ "getMap", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a088fa17f4cf314fe3230f76cfa6275b2", null ],
    [ "setCoordinateX", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#aabf61ebefce14a386be3d420d888bf75", null ],
    [ "setCoordinateY", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a53ad49ed724d108607107d60a2c23d70", null ],
    [ "setId", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#aa68ac563959331622ed3728afa2fee5a", null ],
    [ "setMap", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a7a59f4d2ff60a0c7da0431c7c69a5ee4", null ],
    [ "toString", "classfr_1_1utbm_1_1core_1_1entity_1_1Location.html#a51f26739f1962ff3b1340c52cf8e332e", null ]
];