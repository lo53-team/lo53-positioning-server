var NAVTREE =
[
  [ "LO53-PositioningServer", "index.html", [
    [ "Getting started", "index.html", [
      [ "Introduction", "index.html#intro_sec", null ],
      [ "Installation", "index.html#install_sec", [
        [ "Prerequisites", "index.html#prerequisites", null ],
        [ "Step1 : Clone the repository", "index.html#step1", null ],
        [ "Step2 : Configure database", "index.html#step2", null ],
        [ "Step3 : Set the virtual machine IP address", "index.html#step3", null ],
        [ "Step4 : Start the server", "index.html#step4", null ]
      ] ]
    ] ],
    [ "README", "md_README.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';