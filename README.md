\mainpage Getting started

\section intro_sec Introduction

This is the documentation of our positioning server. Needs extra 0.5dh to write it.

\section install_sec Installation

\subsection prerequisites Prerequisites

* Vm debian
* Apache tomcat 8
* Maven
* PostgreSQL

\subsection step1 Step1 : Clone the repository

Clone the repository by using the following command :

> `git clone git@bitbucket.org:lo53-team/lo53-positioning-server.git`

\subsection step2 Step2 : Configure database

Open the file located at `core/src/main/resources/hibernate.cfg.xml` and edit these databases attributes :
* Username : hibernate.connection.username
* Password : hibernate.connection.password
* String url : hibernate.connection.url

``` xml
<hibernate-configuration>
    <session-factory>

        <property name="hibernate.connection.username">lo53_ips</property>
        <property name="hibernate.connection.password">LO53</property>
        <property name="hibernate.connection.url">jdbc:postgresql://localhost/lo53_ips?verifyServerCertificate=false&amp;useSSL=false&amp;requireSSL=false</property>

    </session-factory>
</hibernate-configuration>
```

\subsection step3 Step3 : Set the virtual machine IP address

Set a static IP address to the VM

\subsection step4 Step4 : Start the server

Start the server by using Maven

> `mvn deploy`

